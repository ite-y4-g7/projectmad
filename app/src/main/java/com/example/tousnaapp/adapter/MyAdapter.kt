package com.example.tousnaapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.example.tousnaapp.databinding.ActivityItemPnhompenhBinding
import com.example.tousnaapp.model.News

class MyAdapter(private var newList: List<News>) :
    RecyclerView.Adapter<MyAdapter.LanguageViewHolder>() {

    inner class LanguageViewHolder(val binding: ActivityItemPnhompenhBinding) :
        RecyclerView.ViewHolder(binding.root)

    fun setFilteredList(mList: List<News>) {
        this.newList = mList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LanguageViewHolder {
        val binding = ActivityItemPnhompenhBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return LanguageViewHolder(binding)
    }

    override fun onBindViewHolder(holder: LanguageViewHolder, position: Int) {
        val currentItem = newList[position]
        holder.binding.Categories1.setImageResource(currentItem.image)
    }

    override fun getItemCount(): Int {
        return newList.size
    }
}

private fun CardView.setImageResource(image: Int) {

}
